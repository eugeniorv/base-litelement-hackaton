import { LitElement } from "lit-element";

class ClientsDm extends LitElement{

  static get properties(){
    return {
      clients: {type: Array},
      newClient: {type: Object},
      modifyClient: {type: Object},
      deleteClientId: {type: String}
    };
  }

  constructor(){
    super();

    console.log("constructor clients-dm");

    //this.getClientsData();

    this.clients = [
      {
          "id": "1",
          "name": "Usuario 1",
          "age": 10
      },
      {
          "id": "2",
          "name": "Usuario 2",
          "age": 20
      },
      {
          "id": "3",
          "name": "Usuario 3",
          "age": 30
      }
    ]

  }

  updated(changedProperties){
    console.log("updated en clients-dm");

    if (changedProperties.has("clients")){
      console.log("ha cambiado valor de clients");

      this.dispatchEvent(
        new CustomEvent(
          "clients-data-updated",
          {
            detail :{
              clients : this.clients
            }
          }
        )
      );
    }

    if (changedProperties.has("deleteClientId")){
      console.log("ha cambiado valor de clients en clients-main");
      this.deleteClient();
    }

    if (changedProperties.has("newClient")){
      console.log("ha cambiado valor de newClient en clients-main");
      this.getNewClient();
    }

    if (changedProperties.has("modifyClient")){
      console.log("ha cambiado valor de modifyClient en clients-main");
      this.getmodifyClient();
    }

  }

  getClientsData(){
    console.log("Estoy en getClientsData");
    console.log("Obteniendo datos de los clientes");

    let xhr = new XMLHttpRequest();

    xhr.onload = () => {
      if (xhr.status === 200){
        console.log("Petición completada correctamente");

        console.log(JSON.parse(xhr.responseText));
        let APIResponse = JSON.parse(xhr.responseText);

        if (APIResponse){
          this.clients = APIResponse;
        }

      }
    }

    xhr.open("GET", "http://localhost:8080/hackaton/clients");
    xhr.send();

    console.log("Fin de getClientsData");
  }

  deleteClient(){
    console.log("deleteClient en clients-dm");
    console.log("se va a eliminar el cliente con Id = " + this.deleteClientId);

    let xhr = new XMLHttpRequest();

    xhr.onload = () => {
      if (xhr.status === 200){
        console.log("Petición completada correctamente");
        this.getClientsData();
      }
    }

    xhr.open("DELETE", "http://localhost:8080/hackaton/clients/" + this.deleteClientId);
    xhr.send();

    console.log("Fin de deleteClient");
  }

  getNewClient(){
    console.log("getNewClient en clients-dm");
    console.log("se va a dar de alta el cliente: " + JSON.stringify(this.newClient));

    let xhr = new XMLHttpRequest();

    xhr.onload = () => {
      if (xhr.status === 200){
        console.log("Petición completada correctamente");
        this.getClientsData();
      }
    }

    xhr.open("POST", "http://localhost:8080/hackaton/clients");
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.send(JSON.stringify(this.newClient));

    console.log("Fin de getNewClient");
  }

  getmodifyClient(){
    console.log("getmodifyClient en clients-dm");
    console.log("se va modificar el cliente: " + JSON.stringify(this.modifyClient));

    let xhr = new XMLHttpRequest();

    xhr.onload = () => {
      if (xhr.status === 200){
        console.log("Petición completada correctamente");
        this.getClientsData();
      }
    }

    xhr.open("PUT", "http://localhost:8080/hackaton/clients/" + this.modifyClient.id);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.send(JSON.stringify(this.modifyClient));

    console.log("Fin de getmodifyClient");
  }

}

customElements.define('clients-dm', ClientsDm);
