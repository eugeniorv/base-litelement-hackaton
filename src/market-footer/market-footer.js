import { LitElement, html } from "lit-element";

class MarketFooter extends LitElement{

  static get properties(){
    return {
    };
  }

  constructor(){
    super();
  }

  render(){
    return html`
      <h5>@MarketApp 2021</h5>
    `;
  }
}

customElements.define('market-footer', MarketFooter);
