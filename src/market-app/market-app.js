import { LitElement, html } from "lit-element";
import '../market-header/market-header.js';
import '../market-sidebar/market-sidebar.js';
import '../clients-main/clients-main.js';
import '../market-footer/market-footer.js';

class MarketApp extends LitElement{

  static get properties(){
    return {
    };
  }

  constructor(){
    super();
  }

  render(){
    return html`
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <market-header></market-header>
    <div class="row">
      <market-sidebar class="col-2" @new-cliente="${this.newClient}"></market-sidebar>
      <clients-main class="col-10"></clients-main>
    </div>
    <market-footer></market-footer>
  `;
  }

  newClient(e) {
    console.log("market-app.js - newClient");
    console.log("market-app.js - Se va a crear un nuevo cliente");
    this.shadowRoot.querySelector("clients-main").showClientForm = true;

  }
}

customElements.define('market-app', MarketApp);
