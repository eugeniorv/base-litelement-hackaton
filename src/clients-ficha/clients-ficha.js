import { LitElement, html } from "lit-element";

class ClientsFicha extends LitElement{

  static get properties(){
    return {
      id : {type: String},
      name: {type: String},
      age : {type: Number}
    };
  }

  constructor(){
    super();
    this.id = "";
    this.name = "";
    this.age = "";

  }

  render(){
    return html`
      <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
      <div class="card h-100">
        <div class="card-body">
          <h5 class="card-title">${this.name}</h5>
          <ul class="list-group list-group-flush">
            <li class="list-group-item">Edad ${this.age}</li>
          </ul>
        </div>
        <div class="card-footer">
          <button class="btn btn-danger col-5" @click="${this.deleteClient}"><strong>X</strong></button>
          <button class="btn btn-info col-5 offset-1" @click="${this.InfoClient}"><strong>Info</strong></button>
        </div>
      </div>
    `;
  }

  deleteClient(e){
    console.log("deleteClient en clients-ficha");
    console.log("Se va a borrar el cliente de nombre " + this.name + " con Id " + this.id);

    this.dispatchEvent(
      new CustomEvent(
        "delete-client",
        {
          "detail" : {
            id: this.id
          }
        }
      )
    );
  }

  InfoClient(e){
    console.log("moreInfo en clients-ficha");
    console.log("Se ha pedido más información del cliente " + this.name + " con Id " + this.id);

    this.dispatchEvent(
      new CustomEvent(
        "info-client",
        {
          "detail" : {
            id: this.id
          }
        }
      )
    );
  }


}

customElements.define('clients-ficha', ClientsFicha);
