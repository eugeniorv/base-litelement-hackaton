import { LitElement, html } from "lit-element";

class MarketHeader extends LitElement{

  static get properties(){
    return {
    };
  }

  constructor(){
    super();
  }

  render(){
    return html`
      <h1>Car Market</h1>
    `;
  }
}

customElements.define('market-header', MarketHeader);
