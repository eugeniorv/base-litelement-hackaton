import { LitElement, html } from "lit-element";

class MarketSidebar extends LitElement{



  updated(changedProperties){
    console.log("updated en market-sidebar");
  }

  render(){
    return html`
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <aside>
      <section>
        <div class="mt-5">
          <button @click="${this.listClients}" class="w-50 btn btn-success" style="font-size: 20px"><strong>Clientes</strong></button>
        </div>
        <div class="mt-5">
          <button @click="${this.newClient}" class="w-50 btn btn-success" style="font-size: 20px"><strong>Alta Cliente</strong></button>
        </div>
      </section>
    </aside>
    `;
  }

  listClients(e){
    console.log("listClients");

    //this.dispatchEvent(new CustomEvent("new-person", {}));
  }

  newClient(e) {
    console.log("market-sidebar.js - newClient");
    console.log("market-sidebar.js - Se va a crear un nuevo cliente");

  this.dispatchEvent(new CustomEvent("new-cliente", {}));
  }
}

customElements.define('market-sidebar', MarketSidebar);
